﻿using System.Collections.Generic;

namespace Yuki.Bot.Services.Localization
{
    public class URLStrings
    {
        public string donation_url { get; set; }
        public string bot_invite_url { get; set; }
        public string server_invite_url { get; set; }
        public string source_url { get; set; }
        public List<string> goodnight { get; set; }
    }
}