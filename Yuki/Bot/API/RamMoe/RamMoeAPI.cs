﻿namespace Yuki.Bot.API.RamMoe
{
    public class RamMoeAPI
    {
        public string path { get; set; }
        public string id { get; set; }
        public string type { get; set; }
        public bool nsfw { get; set; }
    }
}