﻿namespace Yuki.Bot.Common.Events
{
    public enum GuildEvent
    {
        USER_JOIN,
        USER_LEAVE,
        MESSAGE_EDIT,
        MESSAGE_DELETE,
        USER_BAN,
        USER_BAN_CMD,
        USER_UNBAN,
        USER_KICK,
        USER_WARNING_ADDED,
        USER_WARNING_REMOVED,
        USER_MUTE,
        USER_UNMUTE,
        NONE
    }

}
