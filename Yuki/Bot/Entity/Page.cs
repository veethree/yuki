﻿namespace Yuki.Bot.Entity
{
    public class Page
    {
        public int DataOnPage { get; set; }
        public string Value { get; set; }
    }
}
